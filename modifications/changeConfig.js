const fs = require('fs');

const changeFile = (f, c) => {
  if (!f || !c || typeof c !== 'function') return;

  if (!fs.existsSync(f)) {
    console.error(`Cannot find file: ${f}`);
    return;
  }

  const content = fs.readFileSync(f, 'utf8');
  const newContent = c(content);

  fs.writeFileSync(f, newContent);
}

module.exports = (p) => {
  let { rootDir, beModules, feModules, siteCodeName, siteName, ICP, copyRight } = p;

  if (!rootDir) {
    throw 'No root directory specified!';
  }

  beModules = beModules.map(bm => bm.replace(/^.*gitlab\.com\/eis\-base\/modules\//g, '').replace(/\.git$/g, ''));
  feModules = feModules.map(fm => fm.replace(/^.*gitlab\.com\/eis\-base\/modules\//g, '').replace(/\.git$/g, ''));

  // console.log(beModules, feModules);

  // when have demo pro, change dev config to include demo pro module
  // when have demo module, change dev config to include demo module
  // const beConfigModules = [];
  // const feConfigModules = [];
  const beDevConfig = `${rootDir}/be/config/config.development.js`;
  const feDevConfig = `${rootDir}/fe/src/config/config.development.js`;

  // if (beModules.indexOf('eis-module-mongodb') >= 0){
  //     beConfigModules.push({
  //         name: 'db',
  //         path: 'eis-module-mongodb',
  //     });
  // }

  // if(siteCodeName) {
  //     beConfigModules.push(siteCodeName.replace(/\s/g, '-'));
  //     feConfigModules.push(siteCodeName.replace(/\s/g, '-'));
  // }

  let rootModule = '';
  if (beModules.indexOf('eis-module-demo-pro') >= 0) {
    // beConfigModules.push('demo-pro');
    // feConfigModules.push('demo-pro');
    rootModule = 'demo-pro';
  } else if (beModules.indexOf('eis-module-demo') >= 0) {
    // beConfigModules.push('demo');
    // feConfigModules.push('demo');
    rootModule = 'demo';
  }

  changeFile(beDevConfig, () => {
    let configStr = `const path = require('path');

      module.exports = {
      modules: [
        ${beModules.filter(bm => bm !== siteCodeName && bm !== 'eis-module-core')
            .map(bm => {
              if (bm === 'eis-module-mongodb') {
                return {
                  name: 'db',
                  path: 'eis-module-mongodb'
                };
              }

              return typeof bm === 'string' ? `'${bm}'` : `'${bm.name}'`
            })
            .map(bm => {
              return typeof bm === 'string' ? bm.replace(/eis-module-/g, '') : JSON.stringify(bm);
            }).join(',\n    ')}
      ],
      staticFolders: [path.join(__dirname, "../../uploadfiles")],
    `;

    // add module configs
    if (beModules.indexOf('eis-module-mongodb') >= 0) {
      configStr += `
        db: {
          forceDate: true,
          development: {
            name: 'eis_db_dev100'
          }
        },`;
    }

    if (beModules.indexOf('eis-module-sms') >= 0) {
      configStr += `
        sms: {
          cacheTime: 60 * 1000,
          platform: 'eis',
          fixedCode: '1234'
        },`;
    }

    if (beModules.indexOf('eis-module-passport') >= 0) {
      configStr += `
        passport: {
          accountDefaultPermissions: {
            uc: {
              info: {
              }
            }
          },
          whiteList: [
            '/api/eis/ensure_eis_modules',
            /ensure_eis_modules\\/.*$/,
            /^\\/assets\\/.*$/,
            '/api/eis/restart',
            '/api/recover',
            /^\\/api\\/register(\\/.*)?$/,
            '/api/mourning',
          ],
          userWhiteList: [
            '/api/can_i',
            '/api/logedin',
            /^\\/api\\/menu\\/menus(\\/)?.*$/,
          ],
        },`;
    }

    if (beModules.indexOf('eis-module-system-log') >= 0) {
      configStr += `
        'system-log': {
          ignoreList: [
            /^\\/assets\\/.*$/,
            /^\\/api\\/nothing$/,
            '/api/can_i',
            '/api/mourning',
            /^\\/api\\/menu\\/menus\\?.*$/
          ]
        },`;
    }

    // close
    configStr += `};`;

    return configStr;
  });
  changeFile(feDevConfig, () => {
    let configStr = `
      export default {
        modules: [
              ${feModules.filter(fm => fm !== siteCodeName && fm !== 'eis-admin-core')
              .map(fm => typeof fm === 'string' ? `'${fm}'` : `'${fm.name}'`)
              .map(fm => {
                return fm.replace(/eis-admin-/g, '');
              }).join(',\n    ')}
        ],`;

    if(rootModule) {
      configStr += `
        rootModule: '${rootModule}',`;
    }

    if(feModules.indexOf() >= 0) {
      configStr += `
        'demo-pro': {
          siteName: '${siteName}',
          ICP: '${ICP || ''}',
          copyRight: '${copyRight || ''}',
        },`;
    }

    configStr += `};`;

    return configStr;
  });
}