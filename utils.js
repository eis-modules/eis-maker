const fs = require('fs');
const path = require('path');

const allLocales = [];
const files = fs.readdirSync('./i18n', {
    withFileTypes: true
});

for (let i = 0; i < files.length; i += 1) {
    const f = files[i];

    if (f.isDirectory()) {
        let locale = require(path.resolve('./i18n', f.name));
        if (locale) {
            allLocales[f.name] = locale;
        }
    }
}

module.exports = {
    t: (l) => (t) => {
        l = l || 'zh-cn';

        const locale = allLocales[l];

        return (locale && locale[t]) || t;
    }
}