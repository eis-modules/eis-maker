const fs = require('fs');

module.exports = (p) => {
    let { rootDir, beModules, feModules, siteCodeName } = p;

    // TODO: not include modules in customzied module yet
    beModules = [];
    feModules = [];

    if (!rootDir) {
        throw 'No root directory specified!';
    }

    beModules = beModules.map(bm => bm.replace(/^.*gitlab\.com\/eis\-base\/modules\//g, '').replace(/\.git$/g, ''));
    feModules = feModules.map(fm => fm.replace(/^.*gitlab\.com\/eis\-base\/modules\//g, '').replace(/\.git$/g, ''));

    siteCodeName = siteCodeName.replace(/\s/g, '-');

    // create module folder
    fs.mkdirSync(`${rootDir}/be/modules/${siteCodeName}`);
    fs.mkdirSync(`${rootDir}/fe/src/modules/${siteCodeName}`);

    fs.writeFileSync(`${rootDir}/be/modules/${siteCodeName}/index.js`, `module.exports = {
    config:{
        dependencies: [
            ${beModules.filter(bm=>bm !== siteCodeName && bm !== 'eis-module-core' && bm !== 'eis-module-mongodb').map(bm => typeof bm === 'string' ? `'${bm}'` : `'${bm.name}'`).map(bm => {
                return bm.replace(/^eis-module-/g, '');
            }).join(',\n    ')}
        ]
    },
}
    `);
    fs.writeFileSync(`${rootDir}/fe/src/modules/${siteCodeName}/index.js`, `export default {
    config:{
        dependencies: [
            ${feModules.filter(fm => fm !== siteCodeName && fm !== 'eis-admin-core').map(fm => typeof fm === 'string' ? `'${fm}'` : `'${fm.name}'`).map(fm => {
                return fm.replace(/^eis-admin-/g, '');
            }).join(',\n    ')}
        ]
    },
}
    `);
}