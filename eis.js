/**
 * Generate the EIS frontend and backend projects
 */
const MODULE_PATH = './modules';

const fs = require('fs');
const path = require('path');
const exec = require('child_process').exec;
const execSync = require('child_process').execSync;
const inquirer = require('inquirer');

let DEBUG = true;
let locale = 'zh-cn';
let AutoRun = false;
let Customize = false;

const args = process.argv.splice(2);

if (args && args.length) {
    for (let i = 0; i < args.length; i += 1) {
        const arg = args[i];
        const aList = arg.split('=');

        if (aList && aList.length) {
            switch (aList[0]) {
                case 'locale':
                    locale = aList[1].trim() || locale;
                    break;
                case 'prod':
                    DEBUG = false;
                    break;
                case 'run':
                    AutoRun = true;
                    break;
                case 'customize':
                    Customize = true;
                    break;
                default:
                    break;
            }
        }
    }
}

const T = require('./utils.js').t(locale);

// load modules
if (!fs.existsSync(MODULE_PATH)) {
    console.error(T('Cannot find the modules folder'));
    process.exit(-1);
}

inquirer.prompt([
    {
        name: 'siteName',
        type: 'input',
        message: T('Input the name of your system: '),
    },
    {
        name: 'siteCodeName',
        type: 'input',
        message: T('Input the code name of your system: '),
    },
    {
        name: 'ICP',
        type: 'input',
        message: T('Input the ICP number: '),
    },
    {
        name: 'copyRight',
        type: 'input',
        message: T('Input the copy right infomation: '),
    },
    {
        name: 'rootDir',
        type: 'input',
        message: T('Input the path to store the projects: '),
        validate: (input) => {
            return input && fs.existsSync(input) || T('Please make sure the path is exists!');
        }
    },
    {
        name: 'deleteExists',
        type: 'confirm',
        message: T('Delete the exists be and fe folders? (default Y) '),
        default: true,
        when: (input) => {
            return input && input.rootDir && (fs.existsSync(`${input.rootDir}/be`) || fs.existsSync(`${input.rootDir}/fe`));
        }
    },
    {
        name: 'exit',
        type: 'confirm',
        message: () => {
            process.exit(0);
        },
        when: (input) => {
            return input && input.rootDir && input.deleteExists === false;
        },
    },
    {
        name: 'useStoredAccount',
        type: 'confirm',
        message: T('Git account stored already? (default Y) '),
        default: true
    },
    {
        name: 'gitUserName',
        type: 'input',
        message: T('User name of your gitlab account: '),
        when: (input) => {
            return input && !input.useStoredAccount;
        }
    },
    {
        name: 'gitUserPwd',
        type: 'password',
        message: T('Password of your gitlab account: '),
        when: (input) => {
            return input && !input.useStoredAccount;
        }
    },
    ...(Customize ? [{
        name: 'modules',
        type: 'checkbox',
        message: T('Which modules do you need? '),
        choices: (input) => {
            let { gitUserName, gitUserPwd } = input;

            if (gitUserName) {
                gitUserName = gitUserName.replace('@', '%40');
            }

            const allModules = [];
            const files = fs.readdirSync(MODULE_PATH, {
                withFileTypes: true
            });

            for (let i = 0; i < files.length; i += 1) {
                const f = files[i];

                if (!f.isDirectory()) {
                    if (path.extname(f.name) === '.js') {
                        let modules = require(path.resolve(MODULE_PATH, f.name));
                        if (modules) {
                            if (typeof modules === 'function') {
                                modules = modules(gitUserName, gitUserPwd);
                            }

                            if (Array.isArray(modules)) {
                                for (let j = 0; j < modules.length; j += 1) {
                                    const mdl = modules[j];

                                    if (allModules.findIndex(md => md.name === mdl.name) < 0) {
                                        allModules.push({...mdl, name: T(mdl.name)});
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return allModules;
        }
    }] : []),
]).then(answers => {
    let { rootDir, gitUserName, gitUserPwd, modules, deleteExists, siteName, siteCodeName, ICP, copyRight } = answers;

    if (gitUserName) {
        gitUserName = gitUserName.replace('@', '%40');
    }

    if(!modules) {
        const builtIn = require('./modules/builtIn');
        builtIn(gitUserName, gitUserPwd);
        modules = builtIn.allModules() || [];
    }

    function execute (cmd, sync = true) {
        if (sync) {
            try {
                execSync(cmd, DEBUG ? { stdio: 'inherit' } : {});
            } catch (error) {
                console.error(error);
                process.exit(-1);
            }
        } else {
            exec(cmd, DEBUG ? { stdio: 'inherit' } : {}, function (error, stdout, stderr) {
                if (error) {
                    console.error(error);
                    process.exit(-1);
                }
            });
        }
    }

    const unPwd = gitUserName && gitUserPwd ? `${gitUserName}:${gitUserPwd}@` : ''

    // Backend commands
    let beCommands = [];

    if (deleteExists) {
        beCommands.push(`rm -rf ${rootDir}/be`);
    }

    beCommands = beCommands.concat([
        `git clone https://${unPwd}gitlab.com/eis-modules/eis-module-starter-kit.git ${rootDir}/be`,
        `cd ${rootDir}/be`,
        'rm -rf .git',
    ])

    // Frontend commands
    let feCommands = [];

    if (deleteExists) {
        feCommands.push(`rm -rf ${rootDir}/fe`);
    }

    feCommands = feCommands.concat([
        `git clone https://${unPwd}gitlab.com/eis-modules/eis-admin-starter-kit.git ${rootDir}/fe`,
        `cd ${rootDir}/fe`,
        'rm -rf .git',
    ]);

    // add selected modules
    let beModules = [];
    let feModules = [];
    for (let i = 0; i < modules.length; i += 1) {
        const mdl = modules[i];

        const beMdls = mdl.backend && (Array.isArray(mdl.backend) ? mdl.backend : [mdl.backend]).map(mdl => {
            if (typeof mdl === 'function') {
                return mdl(gitUserName, gitUserPwd);
            } else {
                return mdl;
            }
        });
        const feMdls = mdl.frontend && (Array.isArray(mdl.frontend) ? mdl.frontend : [mdl.frontend]).map(mdl => {
            if (typeof mdl === 'function') {
                return mdl(gitUserName, gitUserPwd);
            } else {
                return mdl;
            }
        });

        if (beMdls) {
            beMdls.forEach(bm => {
                if (beModules.indexOf(bm) < 0) {
                    beModules.push(bm);
                }
            });
        }
        if (feMdls) {
            feMdls.forEach(fm => {
                if (feModules.indexOf(fm) < 0) {
                    feModules.push(fm);
                }
            });
        }
    }

    // if no modules selected, add core modules
    if (beModules.length <= 0) {
        beModules = [
            'eis-module-core',
        ];
    }
    if (feModules.length <= 0) {
        feModules = [
            'eis-admin-core',
        ];
    }

    beCommands.push(`yarn add ${beModules.join(' ')}`);
    feCommands.push(`yarn add ${feModules.join(' ')}`);

    // install other dependencies
    beCommands = beCommands.concat([
        `yarn install -f`,
    ])

    feCommands = feCommands.concat([
        `yarn install -f`,
    ]);

    // run main commands
    console.log(T('Cloneing be starter kit and installing modules...'));
    execute(beCommands.join(' && '));

    console.log(T('Cloneing fe starter kit and installing modules...'));
    execute(feCommands.join(' && '), true);

    // run modifications
    const files = fs.readdirSync('./modifications', {
        withFileTypes: true
    });

    for (let i = 0; i < files.length; i += 1) {
        const f = files[i];

        if (!f.isDirectory()) {
            if (path.extname(f.name) === '.js') {
                let modification = require(path.resolve('./modifications', f.name));
                if (modification && typeof modification === 'function') {
                    modification({ modules, rootDir, beModules, feModules, siteName, siteCodeName, ICP, copyRight });
                }
            }
        }
    }

    // lint
    execute(`cd ${rootDir}/fe && yarn lint:fix`, true);
    execute(`cd ${rootDir}/be && yarn lint:fix`, true);

    if (AutoRun) {
        // start
        console.log(T('Starting backend...'));
        execute(
            [
                `cd ${rootDir}/be`,
                'yarn start'
            ].join(' && '),
            false
        );

        console.log(T('Starting frontend...'));
        execute(
            [
                `cd ${rootDir}/fe`,
                'yarn start'
            ].join(' && '),
            false
        );
    }

    console.log(T('================== Done! =================='));
    process.exit(0);
});
