# EIS-maker
This project is trying to create a script which can be used to generate the backend and frontend projects for EIS system from nothing.

# Usage
1. Download and run the script `eis.js` in NodeJs environment.
2. The script will ask for questions and then generate the projects for you.