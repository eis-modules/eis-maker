let allModules = [];

module.exports = (gitUserName, gitUserPwd) => {
    const unPwd = (gitUserName && gitUserPwd) ? `${gitUserName}:${gitUserPwd}@` : '';
    const pro = (n) => `https://${unPwd}gitlab.com/eis-base/modules/${n.trim()}.git`;

    allModules = [{
        backend: [
            'eis-module-core',
            'eis-module-mongodb',
            'eis-module-development-tools',
            pro('eis-module-account'),
            pro('eis-module-dictionary'),
            pro('eis-module-error-code'),
            pro('eis-module-filehelper'),
            pro('eis-module-flow'),
            pro('eis-module-flow-editor'),
            pro('eis-module-input-field'),
            pro('eis-module-menu'),
            pro('eis-module-mourning'),
            pro('eis-module-org-based-permission-control'),
            pro('eis-module-organization'),
            pro('eis-module-passport'),
            pro('eis-module-permission'),
            pro('eis-module-permission-label'),
            pro('eis-module-redis'),
            pro('eis-module-sms'),
            pro('eis-module-system-config'),
            pro('eis-module-system-log'),
            pro('eis-module-uc'),
            pro('eis-module-validators'),
            'eis-module-demo',
            pro('eis-module-demo-pro'),
        ],
        frontend: [
            'eis-admin-core',
            'eis-admin-development-tools',
            pro('eis-admin-account'),
            pro('eis-admin-account-entry'),
            pro('eis-admin-basic-components'),
            pro('eis-admin-dictionary'),
            pro('eis-admin-error-code'),
            pro('eis-admin-field-components'),
            pro('eis-admin-flow'),
            pro('eis-admin-flow-editor'),
            pro('eis-admin-labels'),
            pro('eis-admin-menu'),
            pro('eis-admin-mixins'),
            pro('eis-admin-mourning'),
            pro('eis-admin-org-based-permission-control'),
            pro('eis-admin-organization'),
            pro('eis-admin-passport'),
            pro('eis-admin-permission'),
            pro('eis-admin-system-config'),
            pro('eis-admin-uc'),
            pro('eis-admin-validators'),
            pro('eis-admin-theme-default'),
            'eis-admin-demo',
            pro('eis-admin-demo-pro'),
        ],
    }];

    return [
        {
            name: 'All built-in modules',
            short: 'All',
            value: allModules,
        },
        {
            name: "Account management",
            short: 'Account',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    pro('eis-module-account'),
                    pro('eis-module-input-field'),
                    pro('eis-module-passport'),
                    pro('eis-module-sms'),
                    pro('eis-module-permission')
                ],
                frontend: [
                    'eis-admin-core',
                    pro('eis-admin-account'),
                    pro('eis-admin-basic-components'),
                    pro('eis-admin-field-components'),
                    pro('eis-admin-mixins'),
                ],
            }
        },
        {
            name: "Dictionary management",
            short: 'Dictionary',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    pro('eis-module-dictionary'),
                ],
                frontend: [
                    'eis-admin-core',
                    pro('eis-admin-dictionary'),
                    pro('eis-admin-mixins'),
                ],
            }
        },
        {
            name: "Error code management",
            short: 'Error code',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    pro('eis-module-error-code'),
                ],
                frontend: [
                    'eis-admin-core',
                    pro('eis-admin-error-code'),
                    pro('eis-admin-basic-components'),
                    pro('eis-admin-mixins'),
                ],
            }
        },
        {
            name: "Flow management",
            short: 'Flow',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    pro('eis-module-flow'),
                    pro('eis-module-flow-editor'),
                    pro('eis-module-account'),
                    pro('eis-module-dictionary'),
                    pro('eis-module-input-field'),
                ],
                frontend: [
                    'eis-admin-core',
                    pro('eis-admin-flow'),
                    pro('eis-admin-flow-editor'),
                    pro('eis-admin-basic-components'),
                    pro('eis-admin-field-components'),
                    pro('eis-admin-mixins'),
                ],
            }
        },
        {
            name: "Organization management",
            short: 'Organization',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    pro('eis-module-organization'),
                    pro('eis-module-account'),
                ],
                frontend: [
                    'eis-admin-core',
                    pro('eis-admin-organization'),
                    pro('eis-admin-mixins'),
                ],
            }
        },
        {
            name: "Passport",
            short: 'Passport',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    pro('eis-module-passport'),
                    pro('eis-module-account'),
                    pro('eis-module-sms'),
                    pro('eis-module-permission')
                ],
                frontend: [
                    'eis-admin-core',
                    pro('eis-admin-passport')
                ],
            }
        },
        {
            name: "Organization based permission control",
            short: 'Organization permission',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    pro('eis-module-org-based-permission-control'),
                    pro('eis-module-passport'),
                    pro('eis-module-account'),
                    pro('eis-module-sms'),
                    pro('eis-module-permission'),
                    pro('eis-module-organization'),
                ],
                frontend: [
                    'eis-admin-core',
                    pro('eis-admin-org-based-permission-control'),
                    pro('eis-admin-organization'),
                ],
            }
        },
        {
            name: "Permission management",
            short: 'Permission',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    pro('eis-module-permission'),
                ],
                frontend: [
                    'eis-admin-core',
                    pro('eis-admin-permission'),
                    pro('eis-admin-mixins'),
                ],
            }
        },
        {
            name: "Permission labels",
            short: 'Label',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    pro('eis-module-permission-label'),
                    pro('eis-module-passport'),
                    pro('eis-module-account'),
                    pro('eis-module-sms'),
                    pro('eis-module-permission')
                ],
                frontend: [
                    'eis-admin-core',
                    pro('eis-admin-labels'),
                    pro('eis-admin-mixins'),
                ],
            }
        },
        {
            name: "System configuration",
            short: 'Configuration',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    pro('eis-module-system-config'),
                ],
                frontend: [
                    'eis-admin-core',
                    pro('eis-admin-system-config'),
                    pro('eis-admin-mixins'),
                ],
            }
        },
        {
            name: "User center",
            short: 'UC',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    pro('eis-module-uc'),
                    pro('eis-module-passport'),
                    pro('eis-module-account'),
                    pro('eis-module-sms'),
                    pro('eis-module-permission'),
                    pro('eis-module-input-field'),
                ],
                frontend: [
                    'eis-admin-core',
                    pro('eis-admin-uc'),
                    pro('eis-admin-passport'),
                    pro('eis-admin-basic-components'),
                    pro('eis-admin-mixins'),
                ],
            }
        },
        {
            name: "Built-in validators",
            short: 'Validators',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    pro('eis-module-validators'),
                ],
                frontend: [
                    'eis-admin-core',
                    pro('eis-admin-validators')
                ],
            }
        },
        {
            name: "Menu management",
            short: 'Menu',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    pro('eis-module-menu'),
                ],
                frontend: [
                    'eis-admin-core',
                    pro('eis-admin-menu'),
                    pro('eis-admin-mixins'),
                ],
            }
        },
        {
            name: "Account entry (login, logout etc.)",
            short: 'Account entry',
            value: {
                frontend: [
                    pro('eis-admin-account-entry'),
                    pro('eis-admin-passport'),
                    pro('eis-admin-mixins'),
                ]
            }
        },
        {
            name: "Built-in basic components",
            short: 'Basic components',
            value: {
                frontend: [
                    'eis-admin-core',
                    pro('eis-admin-basic-components'),
                    pro('eis-admin-mixins'),
                ],
            }
        },
        {
            name: "Built-in field components",
            short: 'Field components',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    pro('eis-module-input-field'),
                ],
                frontend: [
                    'eis-admin-core',
                    pro('eis-admin-field-components'),
                    pro('eis-admin-mixins'),
                ],
            }
        },
        {
            name: "File helper utilities",
            short: 'File utils',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    pro('eis-module-filehelper'),
                ],
            }
        },
        {
            name: "Redis cache",
            short: 'Redis',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    pro('eis-module-redis'),
                ],
            }
        },
        {
            name: "SMS",
            short: 'SMS',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    pro('eis-module-sms'),
                ],
            }
        },
        {
            name: "System logs",
            short: 'Logs',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    pro('eis-module-system-log'),
                ],
            }
        },
        {
            name: "Demo module",
            short: 'Demo',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    'eis-module-demo',
                ],
                frontend: [
                    'eis-admin-core',
                    'eis-admin-demo',
                ],
            }
        },
        {
            name: "Professional version demo module",
            short: 'Demo pro',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    pro('eis-module-demo-pro'),
                ],
                frontend: [
                    'eis-admin-core',
                    pro('eis-admin-demo-pro')
                ],
            }
        },
        {
            name: 'Default themes',
            short: 'Themes',
            value: {
                frontend: [
                    'eis-admin-core',
                    pro('eis-admin-theme-default'),
                ]
            }
        },
        {
            name: 'Development tools',
            short: 'Dev tools',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    'eis-module-development-tools',
                ],
                frontend: [
                    'eis-admin-core',
                    'eis-admin-development-tools',
                ]
            }
        },
        {
            name: 'Mourning style',
            short: 'Mourning',
            value: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    'eis-module-mourning',
                ],
                frontend: [
                    'eis-admin-core',
                    'eis-admin-mourning',
                ]
            }
        },
        {
            name: 'Online Payment',
            short: 'Online Payment',
            values: {
                backend: [
                    'eis-module-core',
                    'eis-module-mongodb',
                    'eis-module-olpay',
                ],
                frontend: [
                    'eis-admin-core',
                ]
            }
        }
    ];
};

module.exports.allModules = () => allModules;
